// swift-tools-version:5.5
import PackageDescription

let package = Package(
    name: "SimpleC2PA",
    platforms: [
        .iOS(.v15),
    ],
    products: [
        .library(
            name: "SimpleC2PA",
            targets: ["SimpleC2PA"]
        ),
    ],
    dependencies: [],
    targets: [
        .binaryTarget(
            name: "SimpleC2PAFramework",
            path: "SimpleC2PA.xcframework"
        ),
        .target(
            name: "SimpleC2PA",
            dependencies: [ "SimpleC2PAFramework" ]
        ),
    ]
)
